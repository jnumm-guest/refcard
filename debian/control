Source: refcard
Section: doc
Priority: optional
Maintainer: Debian Documentation Project <debian-doc@lists.debian.org>
Uploaders: W. Martin Borgert <debacle@debian.org>,
           Holger Wansing <holgerw@debian.org>
Standards-Version: 4.1.4
Build-Depends: dblatex,
               debhelper (>= 11),
               dia,
               docbook-xsl,
               fonts-hanazono,
               fonts-nakula,
               fonts-ipaexfont-gothic,
               fonts-ipafont-gothic,
               fonts-ipafont-mincho,
               ghostscript,
               lmodern,
               texlive-extra-utils,
               pdftk,
               po4a,
               poppler-utils,
               texlive-lang-cjk,
               texlive-lang-cyrillic,
               texlive-lang-czechslovak,
               texlive-lang-european,
               texlive-lang-french,
               texlive-lang-german,
               texlive-lang-greek,
               texlive-lang-italian,
               texlive-lang-other,
               texlive-lang-polish,
               texlive-lang-portuguese,
               texlive-lang-spanish,
               texlive-xetex,
               xmlroff,
               xsltproc
Vcs-Git: https://salsa.debian.org/ddp-team/refcard.git
Vcs-Browser: https://salsa.debian.org/ddp-team/refcard
Homepage: https://www.debian.org/doc/user-manuals#refcard

Package: debian-refcard
Architecture: all
Depends: ${misc:Depends}
Recommends: evince | pdf-viewer
Suggests: doc-base
Multi-Arch: foreign
Description: printable reference card for the Debian system
 The Debian reference card provides new users help with
 the most important commands. Basic knowledge of computers, files,
 directories and the command line is required, however. The
 package contains printable PDF files in multiple languages.
